#!/usr/bin/env python3

# Example program for converting a Deutsche Post provided ppl_vXXX.csv file
# to JSON. See also the data package sub-directory.
#
# Example call:
#
#     $ update_products.py ppl_v450.csv inema/data/products.json \
#                                       inema/data/products-2020-01-01.json
#
#
# Note: PPL stands for Product Price List

import sys
import json
import csv
import decimal

def quantize(x):
    x = decimal.Decimal(x).quantize(decimal.Decimal('.01'))
    return str(x)

def main(csv_filename, json_filename, filename):
    with open(csv_filename, newline='', encoding='latin-1') as f \
            , open(json_filename) as g \
            , open(filename, 'w') as h:
        rows = csv.DictReader(f, delimiter=';')
        products = json.load(g)

        seen_ids = set()
        for row in rows:
            key, price = row['PROD_ID'], row['PROD_BRPREIS'].replace(',', '.')
            inter, weight, name = (row['PROD_AUSR'] in ('1', 'I')), row['MAXG'], row['PROD_NAME']
            seen_ids.add(key)
            if key in products:
                p = products[key]
                oprice, ointer = p['cost_price'], p['international']
                oweight, oname  = p['max_weight'], p['name']
                # row[5] vs. row[7] => total vs. base product price
                # row[4] vs. row[6] => complete vs. base product name
                price, oprice = quantize(price), quantize(oprice)
                c = False
                if price != oprice:
                    print(f'Price of {name} ({key}) changed: {oprice} -> {price} (from {row["PROD_GUEAB"]})')
                    c = True
                    p['cost_price'] = price
                if inter != ointer:
                    print(f'Nationality of {name} ({key}) changed: {ointer} -> {inter} (from {row["PROD_GUEAB"]})')
                    c = True
                    p['international'] = inter
                if weight != oweight:
                    print(f'Weight of {name} ({key}) changed: {oweight} -> {weight} (from {row["PROD_GUEAB"]})')
                    # as approximation, use old one ...
                    if weight:
                        c = True
                        p['max_weight'] = weight
                # The names returned by the ProdWS might differ from the ones included in the PPL ...
                if name != oname:
                   print(f'Name of {name} ({key}) changed:\n{oname} ->\n{name} (from {row["PROD_GUEAB"]})')
                   c = True
                   p['name'] = name
                if c:
                    print('*'*75)
            else:
                products[key] = {
                        "cost_price": price,
                        "international": inter,
                        "max_weight": weight,
                        "name": name
                        }
                print(f'NEW Product {row[4]} ({row[2]}) - from {row[0]}!')
                print('*'*75)

        remove_ids = set()
        for k in products:
            if k not in seen_ids:
                p = products[k]
                print(f'Product {p["name"]} ({k}) was deleted!')
                remove_ids.add(k)
        for k in remove_ids:
            products.pop(k)

        json.dump(products, h, sort_keys=True, indent=4, ensure_ascii=False)

if __name__ == '__main__':
    sys.exit(main(sys.argv[1], sys.argv[2], sys.argv[3]))
